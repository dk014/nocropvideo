#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>
@import AVKit;
@import AVFoundation;

@protocol VideoDelegate <NSObject>
-(void)videoOverlappingFinished:(NSURL*)videoUrl;
@end

@interface HelperClass : NSObject
- (void)applyFilter:(AVURLAsset*)firstAsset andSecondAsset:(AVURLAsset*)secondAsset onviewController:(UIViewController*)vc andcompos:(AVVideoComposition*)composition Completion:(void(^)(BOOL success, NSError* error, NSURL* videoUrl))completion;
@property (nonatomic, weak) id <VideoDelegate> videodelegateObj;
+ (HelperClass *)sharedHelperClass;

@end
