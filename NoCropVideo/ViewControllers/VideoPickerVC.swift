import UIKit
import Photos
import AVFoundation
import AVKit
import MBProgressHUD



class VideoPickerVC: UIViewController {
    var selectedCellIndex:Int!
    var photos: PHFetchResult<PHAsset>!
    var globalURLToBeSent : URL!
    var globalAsset : Any!
    @IBOutlet weak var videosCollectionView: UICollectionView!
    
    
    @IBAction func backAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion:nil)
    }
    
    
    
}
extension VideoPickerVC{
    override func viewDidLoad(){
        let hud = MBProgressHUD.showAdded(to:self.view, animated: true)
        hud.mode = .indeterminate;
        hud.label.text = "Fetching";
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setUpInitialView()
        
    }
    func setUpInitialView(){
        self.globalURLToBeSent = nil
        checkAuthorizationForPhotoLibraryAndGet()
    }
    
    func getAssetFromPhoto() {
        let options = PHFetchOptions()
        options.sortDescriptors = [ NSSortDescriptor(key: "duration", ascending: true) ]
        let videoPredicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
        options.predicate = videoPredicate
        photos = PHAsset.fetchAssets(with: options)
        if((photos?.count)!>0){
            DispatchQueue.main.async {
                MBProgressHUD.hide(for:self.view, animated:true)
                self.videosCollectionView.reloadData()
            }
        }
    }
    
    func checkAuthorizationForPhotoLibraryAndGet(){
        let status = PHPhotoLibrary.authorizationStatus()
        
        if (status == PHAuthorizationStatus.authorized) {
            getAssetFromPhoto()
            print("a")
        }else {
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                
                if (newStatus == PHAuthorizationStatus.authorized) {
                    print("a")
                    
                    self.getAssetFromPhoto()
                }else {
                    print("a")
                    
                }
            })
        }
    }
}



extension VideoPickerVC:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(photos != nil){
            return photos!.count;
        }else{
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let videoCustomCell = collectionView.dequeueReusableCell(withReuseIdentifier:"videoCell", for: indexPath) as! VideoPickerCustomCell
        if(selectedCellIndex==indexPath.row){
            videoCustomCell.layer.borderWidth = 2
            videoCustomCell.layer.borderColor = UIColor.white.cgColor
        }else{
            videoCustomCell.layer.borderWidth = 0
            videoCustomCell.layer.borderColor = UIColor.white.cgColor
        }
        
        let asset = photos!.object(at: indexPath.row)
        print("asset.description",asset.description)
        let width: CGFloat = 150
        let height: CGFloat = 150
        let size = CGSize(width:width, height:height)
        PHImageManager.default().requestImage(for: asset, targetSize: size, contentMode: PHImageContentMode.aspectFill, options: nil) { (image, userInfo) -> Void in
            videoCustomCell.photoImageView.image = image
        }
        
        
        return videoCustomCell;
    }
    
    
}

extension VideoPickerVC:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 0.0
    }
}



extension VideoPickerVC:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedCellIndex=indexPath.row
        self.videosCollectionView.reloadData()
        
        
        let playVideoVC : PlayVideoController = self.storyboard?.instantiateViewController(withIdentifier:"playVideoVC") as! PlayVideoController
        let asset = photos!.object(at: indexPath.row)
        PHImageManager.default().requestAVAsset(forVideo: asset, options:nil) { (videoUrl,mixObj,userinfo) in
            
            DispatchQueue.main.async {
                if let urlAsset = videoUrl as? AVURLAsset {
                    let localVideoUrl: URL = urlAsset.url as URL
                    self.globalURLToBeSent = localVideoUrl
                    print("globalURLToBeSent",self.globalURLToBeSent)
                    playVideoVC.globalURLReceived = self.globalURLToBeSent
                    
                    self.present(playVideoVC, animated:true, completion:nil)
                } else {
                    
                }
            }
            
            
        }
        
        
    }
    
}






