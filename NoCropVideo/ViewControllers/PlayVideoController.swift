import UIKit
import Photos
import AVKit
import AVFoundation
import CoreImage
import MBProgressHUD
import MediaPlayer

class PlayVideoController: UIViewController {
    var firstAsset:AVURLAsset!
    var secondAsset:AVURLAsset!
    var gradientClick:Bool=false
    var imageClick:Bool=false
    var photosReceived: PHFetchResult<PHAsset>!
    var playerAV: AVPlayer!
    var playerLayerAV:AVPlayerLayer!
    var image : UIImage!
    var frontplayerAV: AVPlayer!
    var frontplayerLayerAV:AVPlayerLayer!
    var composition : AVVideoComposition!
    var avasset:AVAsset!
    var panGesture = UIPanGestureRecognizer()
    var pinchGesture = UIPinchGestureRecognizer()
    var globalFilterName : String!
    
    
    var thumbnailArr = [#imageLiteral(resourceName: "normal"),#imageLiteral(resourceName: "gaussian"),#imageLiteral(resourceName: "Pixellate"),#imageLiteral(resourceName: "Hexagonal"),#imageLiteral(resourceName: "Pointillize"),#imageLiteral(resourceName: "Crystallize"),#imageLiteral(resourceName: "Motion"),#imageLiteral(resourceName: "False-Color")]
  
    var globalURLReceived : URL!
    
   
    @IBOutlet weak var playPauseBtn: UIButton!
    
    @IBOutlet weak var backVideoView: UIView!
    @IBOutlet weak var frontVideoView: UIView!
    @IBOutlet weak var filterScrollView: UIScrollView!
    @IBOutlet weak var bottomView: UIView!

    @IBOutlet weak var backgroundView: UIView!
    
    
    var toggleState = 1
    //MARK:- View life Cycel
    
        override func viewDidLoad() {
            super.viewDidLoad()
            setUpInitialView()
            print("photosReceived",photosReceived)
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
            
        }
    
    //MARK:- Custom methods
    func setUpInitialView(){
        
        self.filterScrollView.isHidden=false
        avasset = AVAsset.init(url:globalURLReceived)
        filterScrollContents()
        applyFilter(globalFilterToBeApplied:"CIVignette")
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(PlayVideoController.draggedView(_:)))
        frontVideoView.isUserInteractionEnabled = true
        frontVideoView.addGestureRecognizer(panGesture)
        
        pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchGestureOnLabel(sender:)))
        frontVideoView.addGestureRecognizer(pinchGesture)
    }
    
    func urlOfCurrentlyPlayingInPlayer(player : AVPlayer) -> URL? {
        return ((player.currentItem?.asset) as? AVURLAsset)?.url
    }
    
    func setUpPlayer(_ abcUrl:URL){
        MBProgressHUD.hide(for:self.view, animated: true)
        let avAsset = AVAsset.init(url:abcUrl)
        let playerItem = AVPlayerItem(asset: avAsset)
        playerItem.videoComposition = composition
        playerAV = AVPlayer(playerItem: playerItem)
        firstAsset=AVURLAsset.init(url:urlOfCurrentlyPlayingInPlayer(player:playerAV)!)
        playerLayerAV = AVPlayerLayer(player: playerAV)
        playerLayerAV.videoGravity = .resize
        playerLayerAV.frame = CGRect.init(x:0, y:0, width:backVideoView.frame.size.width+50, height:backVideoView.frame.size.height)
        backVideoView.layer.addSublayer(playerLayerAV)
        
        
        frontplayerAV = AVPlayer(url: abcUrl)
        frontplayerLayerAV = AVPlayerLayer(player: frontplayerAV)
        frontplayerLayerAV.videoGravity = .resize
        frontplayerLayerAV.frame = CGRect.init(x:0, y:0, width:frontVideoView.frame.size.width, height:frontVideoView.frame.size.height)
        secondAsset=AVURLAsset.init(url:urlOfCurrentlyPlayingInPlayer(player:frontplayerAV)!)
        frontVideoView.layer.addSublayer(frontplayerLayerAV)
        backVideoView.addSubview(frontVideoView)
        frontVideoView.center = backVideoView.center
        frontVideoView.clipsToBounds=true;
        playerAV.seek(to: kCMTimeZero)
        frontplayerAV.seek(to: kCMTimeZero)
        playPauseBtn.setImage(#imageLiteral(resourceName: "play"), for:.normal)
        playerAV.pause()
        frontplayerAV.pause()
        toggleState = 1
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.playerDidFinishPlaying(note:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerAV.currentItem)
        
        
    }
    @objc func playerDidFinishPlaying(note: NSNotification){
        
        playerAV.seek(to: kCMTimeZero)
        frontplayerAV.seek(to: kCMTimeZero)
        playerAV.play()
        frontplayerAV.play()
        toggleState = 2
        
        
    }
    

    //MARK:- Filter Apply Code on backgroundvideo
    func filterScrollContents() {
        var xCoord: CGFloat = 5
        let yCoord: CGFloat = 5
        let buttonWidth:CGFloat = 80
        let gapBetweenButtons: CGFloat = 5
        for i in 0..<thumbnailArr.count{
            let filterButton = UIButton(type: .custom)
            filterButton.frame = CGRect(x: xCoord, y: yCoord, width: 80, height: 80)
            filterButton.tag = i
            filterButton.backgroundColor = UIColor.clear
            filterButton.setTitleColor(UIColor.white, for: .normal)
            filterButton.showsTouchWhenHighlighted = true
            filterButton.setImage(thumbnailArr[i], for:UIControlState.normal)
            filterButton.contentMode = UIViewContentMode.scaleAspectFit
            filterButton.addTarget(self, action:#selector(filterActionTapped), for: .touchUpInside)
            filterButton.layer.cornerRadius = 5
            filterButton.clipsToBounds = true
            xCoord +=  buttonWidth + gapBetweenButtons
            
            filterScrollView.addSubview(filterButton)
        }
        filterScrollView.contentSize = CGSize(width: buttonWidth * CGFloat(thumbnailArr.count)+gapBetweenButtons+30.0, height: 80)
    }
    
    @objc func filterActionTapped(sender:UIButton){
        if(sender.tag==0){
            globalFilterName = "CIVignette"
            applyFilter(globalFilterToBeApplied: globalFilterName)
        }else if(sender.tag==1){
            globalFilterName =  "CIGaussianBlur"
            applyFilter(globalFilterToBeApplied: globalFilterName)
            
        }else if(sender.tag==2){
            globalFilterName = "CIPixellate"
            applyFilter(globalFilterToBeApplied: globalFilterName)
            
            
        }else if(sender.tag==3){
            globalFilterName =  "CIHexagonalPixellate"
            applyFilter(globalFilterToBeApplied: globalFilterName)
            
            
        }else if(sender.tag==4){
            globalFilterName =  "CIPointillize"
            applyFilter(globalFilterToBeApplied: globalFilterName)
            
            
        }else if(sender.tag==5){
            globalFilterName =  "CICrystallize"
            applyFilter(globalFilterToBeApplied: globalFilterName)
            
            
        }else if(sender.tag==6){
            globalFilterName =  "CIMotionBlur"
            applyFilter(globalFilterToBeApplied: globalFilterName)
            
            
            
        }

        else if(sender.tag==7){
            globalFilterName =  "CIFalseColor"
            applyFilter(globalFilterToBeApplied: globalFilterName)
            
        }
        
    }
    
    func applyFilter(globalFilterToBeApplied:String){
        let avAsset =  AVAsset.init(url:globalURLReceived)
        let filter = CIFilter(name:globalFilterToBeApplied)!
        composition = AVVideoComposition(asset: avAsset, applyingCIFiltersWithHandler: { request in
            let source = request.sourceImage.clampedToExtent()
            filter.setValue(source, forKey: kCIInputImageKey)
            if(globalFilterToBeApplied == "CIPixellate"){
                filter.setValue(20, forKey: kCIInputScaleKey)
            } else if(globalFilterToBeApplied == "CIHexagonalPixellate"){
                filter.setValue(20, forKey: kCIInputScaleKey)
            }
            let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
            request.finish(with: output, context: nil)

        })
        self.setUpPlayer(self.globalURLReceived)

    }
    
    //MARK:- Gestures selector method on foreground video
    @objc func pinchGestureOnLabel(sender:UIPinchGestureRecognizer) {
        
        if let view = sender.view {
            view.transform = view.transform.scaledBy(x: sender.scale, y: sender.scale)
            sender.scale = 1
        }
        
    }
    
    
    @objc func draggedView(_ sender:UIPanGestureRecognizer){
        self.view.bringSubview(toFront:frontVideoView)
        let translation = sender.translation(in: backVideoView)
        frontVideoView.center = CGPoint(x: frontVideoView.center.x + translation.x, y: frontVideoView.center.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: self.view)
    }
    
    
    
    //MARK:- BUTTON'S ACTION
    @IBAction func saveAction(_ sender: UIButton) {
        HelperClass.shared().applyFilter(self.firstAsset, andSecondAsset:self.secondAsset, onviewController:self, andcompos:self.composition, completion: { (value,error,url) in
            if(url != nil){
                print("url",url!)
            }
        })
         
        MBProgressHUD.showAdded(to:self.view, animated: true)
    }
    
    
    @IBAction func backAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion:nil)
    }
    
    
    @IBAction func playPauseAction(_ sender: UIButton) {
        
        let playBtn = sender as UIButton
        if toggleState == 1 {
            playerAV.play()
            frontplayerAV.play()
            
            
            toggleState = 2
            playBtn.setTitle("pause", for:.normal)
        } else {
            playerAV.pause()
            frontplayerAV.pause()
            toggleState = 1
            playBtn.setTitle("play", for:.normal)
        }
    }
    
}





