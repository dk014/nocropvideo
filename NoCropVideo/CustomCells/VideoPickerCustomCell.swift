//
//  VideoPickerCustomCell.swift
//  NoCropVideo
//
//  Created by Polak on 6/29/18.
//  Copyright © 2018 Polak. All rights reserved.
//

import UIKit

class VideoPickerCustomCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
}
