//
//  OptionsCustomCell.swift
//  NoCropVideo
//
//  Created by Hutch on 04/07/18.
//  Copyright © 2018 Polak. All rights reserved.
//

import UIKit

class OptionsCustomCell: UICollectionViewCell {
    @IBOutlet weak var optionsImage: UIImageView!
    
    @IBOutlet weak var optionsLable: UILabel!
}
